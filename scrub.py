#!/usr/bin/python3
#
# This program will take in input csv file and clean it up
#
# Use it this way: $ python3 scrub.py -i inputfile.csv -o outputfile.csv
#
# Logic based on: https://medium.com/district-data-labs/simple-csv-data-wrangling-with-python-3496aa5d0a5e
# String formatting: https://docs.python.org/3/library/stdtypes.html#printf-style-string-formatting

import argparse
import configparser
import csv
from pathlib import Path

# Get input and output file names to use
parser = argparse.ArgumentParser( description = "Process input.csv and write as output.csv")
parser.add_argument("--input", "-i", type=str, required=True)
parser.add_argument("--output", "-o", type=str, required=True)

# Read the args from the command line
args = parser.parse_args()
# args.input
# args.output

# Get some config values
config = configparser.ConfigParser()
config.read('scrubconfig.ini')

# Set the input and output folders. This is supposed to account for foward
#  and back slashes for path for Windows compatability
input_folder = Path(config['DEFAULT']['input_folder'])
input_file = input_folder / args.input
output_folder = Path(config['DEFAULT']['output_folder'])
output_file = output_folder / args.output

# This is the object we'll dump the processed rows into for eventual
#  saving as the output.csv
final_output = {}

# Helper function to take a file name and interpret it
# This could use some love
def determine_input_type(name_to_check):
    return name_to_check[0:2]

# Helper function removes whatever characters you send via 'match'
#  and strips blank characters
def remove_chars(input, match):
    final = input.strip(match)
    final = final.strip()
    return final

# Helper function changes line breaks to a single space
def remove_lines(input):
    final = input.replace('\r', ' ').replace('\n', ' ')
    final = final.strip()
    return final

# Helper function removes an item from the object via the key
def pop_item_from_dict(dictionary, key_to_delete):
    #print(dictionary)
    if key_to_delete in dictionary:
        dictionary.pop(key_to_delete)
    else:
        print(f'Key {key_to_delete} is not in the dictionary')


# Function that reads each line from the input file and turns it into
#  a Dictionary object named 'row'
def read_input_data(path):
    with open(path, 'r', encoding='utf-8-sig') as data:
        #DictReader maps the first row into keys aka field names row['fieldname']
        reader = csv.DictReader(data)
        for row in reader:
            yield row


# THis is where the data massaging is taking place. I know using a big
#  if statement is not elegant but it will work for us. Basically each file type we
#  have will get its own if section.
def process_row(row, input_file_type):
    if input_file_type=="xx":
        # This was the first test file xx-input3.csv
        # brand,name,price,thc,cbd,plant-type
        #print("-- in xx processing section")

        # Delete these two objects
        pop_item_from_dict(row,"brand")
        pop_item_from_dict(row,"plant-type")

        # Reformat the price text
        row['price'] = remove_chars(row['price'],"$")
        row['price'] = remove_chars(row['price'],"/ g")

        # Clean up the thc text, then split it into two objects, then delete the original
        row['thc'] = remove_chars(row['thc'],"THC:")
        row['thc'] = remove_chars(row['thc'],"mg")
        x = row['thc'].split("-")
        row['thc-min'] = x[0].strip()
        row['thc-max'] = x[1].strip()
        pop_item_from_dict(row,"thc")

        # Clean up the cbd text, then split it into two objects, then delete the original
        row['cbd'] = remove_chars(row['cbd'],"CBD:")
        row['cbd'] = remove_chars(row['cbd'],"mg")
        y = row['cbd'].split("-")
        row['cbd-min'] = y[0].strip()
        row['cbd-max'] = y[1].strip()
        pop_item_from_dict(row,"cbd")
        return row
    elif input_file_type == "bc":
        # Starting raw header - used file bccs.extracts.csv
        # web-scraper-order,web-scraper-start-url,pagnation,pagnation-href,product-link,
        #  product-link-href,name,thc,cbd,brand,price,size,thc-2,cbd-2,strain,category

        # Delete unwanted columns
        pop_item_from_dict(row,"web-scraper-order")
        pop_item_from_dict(row,"web-scraper-start-url")
        pop_item_from_dict(row,"pagnation")
        pop_item_from_dict(row,"pagnation-href")
        pop_item_from_dict(row,"product-link")
        pop_item_from_dict(row,"product-link-href")
        pop_item_from_dict(row,"category")

        # Clean up name
        row['name'] = remove_lines(row['name'])

        # Reformat the price text
        row['price'] = remove_chars(row['price'],"$")
        row['price'] = remove_chars(row['price'],"/ g")

        return row
    else:
        # Do nothing - just return the row with no processing
        return row

if __name__ == "__main__":

    input_file_type = determine_input_type(args.input)
    print(f"Begin processing input file: {input_file} of type: {input_file_type}")

    # Loop through every row in the input csv file
    for idx, row in enumerate(read_input_data(input_file)):

        # Process the various data within the row
        process_row(row, input_file_type)

        # For debugging - output the row after we've done all the work on it
        #print(f"{idx} row: {row}")

        #fill up another object with the cleansed row data to output later
        final_output[idx] = row

    print(f"Processed a total of {idx+1} rows.")

    # For debugging - what does the output object look like
    #print(final_output)

    #Write the output file
    print(f"Writing to output file: {output_file}")
    with open(output_file, 'w') as csv_file:
        #writer = csv.DictWriter(csv_file, fieldnames=['name','price','thc-min','thc-max','cbd-min','cbd-max'])
        # If all goes well, this should write the first row as the keys (column names) of all your data
        writer = csv.DictWriter(csv_file, fieldnames=final_output[1].keys())
        writer.writeheader()
        for key, value in final_output.items():
           writer.writerow(value)
           #print(value)

    print("End of Program")
